import React from 'react';
import '@testing-library/jest-dom'
import { shallow } from 'enzyme';
import PrimeraApp from "../../PrimeraApp";

describe('pruebas en PrimeraApp', () => { 


  /*  test('debe de mostrar el mensaje "Hola soy Goku" ', () => { 

        const saludo = 'hola, soy goku';
        const {getByText} = render( <PrimeraApp saludo={ saludo } /> )

        expect( getByText(saludo) ).toBeInTheDocument();

        
   })*/

   test(' debe de mostrar <PrimeraApp />  correctamente', () => {
      const saludo = 'Hola soy Goku';
      const wrapper = shallow( <PrimeraApp saludo={saludo}/>)

      expect(wrapper).toMatchSnapshot();
   });

   
   test(' debe de mostrar el subtitulo enviado por props', () => {
      const saludo = 'Hola soy Goku';
      const subTitulo ='Soy un subtitulo';
    
      const wrapper = shallow( 
        <PrimeraApp 
          saludo={ saludo }
          subtitulo={ subTitulo }
        />
      );
      

     const textoParrafo = wrapper.find('p').text();
     //console.log(textoParrafo);//
     expect( textoParrafo ).toBe( subTitulo );

   });
 })