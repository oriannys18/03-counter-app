import '@testing-library/jest-dom';

import { getSaludo } from '../../base/02-template-string';



describe('pruebas en  02-template-string.js', () => {

    test('probar funcion que retorna un hola oriana', () => {

        const nombre = 'oriana';


        const saludo = getSaludo( nombre );

       
       expect(saludo).toBe( 'Hola ' + nombre + '!' );
    })

    test('getSaludo debe retornar Hola Carlos! si no hay argunmento en el nombre', () => {



        const saludo = getSaludo();
       expect( saludo ).toBe( 'Hola Carlos!' );
    })





})




