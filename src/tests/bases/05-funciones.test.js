import '@testing-library/jest-dom'

import {getUser, getUsuarioActivo} from '../../base/05-funciones';



describe('pruebas en la 05-funciones ', () => {

    test('getUser debe de retornar un objeto ', () => { 

        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }

        const user = getUser();

        expect ( user ).toEqual( userTest );
   
        
         })


         test('getUsuarioActivo debe de retornar un objeto ', () => { 

            const nombre = 'oriana'

            const Acti = getUsuarioActivo(nombre);
    
            expect ( Acti).toEqual( {
                
                    uid: 'ABC567',
                    username: nombre
                
            });

            
       
        
             })


});



