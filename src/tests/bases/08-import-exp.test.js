import { getHeroeById, getHeroesByOwner } from '../../base/08-imp-exp';
import  heroes  from '../../data/heroes';


describe('pruebas en la funcion Heroes', () => {


    test('debe de retornar un heroe por id', () => { 
        
        const id = 2;
        const heroe = getHeroeById( id );
       
        const heroeData = heroes.find( h => h.id === id );  

       expect( heroe ).toEqual( heroeData );


    })

    
    test('debe de retornar un undefined si Heroe no existe', () => { 
        
        const id = 10;
        const heroe = getHeroeById(id );

       expect( heroe ).toBe( undefined );


    });

    test('debe de retornar un arreglo con los heroes de DC', () => { 
        
        const owner = 'DC';
        const heroe = getHeroesByOwner(owner );
        

        const Dataowner = heroes.filter( h => h.owner === owner ); 

       expect( heroe ).toEqual( Dataowner );


    });

    test('debe de retornar un arreglo con los heroes de Marvel', () => { 
        
        const owner = 'Marvel';
        const heroe = getHeroesByOwner( owner );
    

       expect( heroe.length ).toBe( 2 );


    });
    
})