import React from 'react';
import { shallow } from 'enzyme'

import { CounterApp } from '../../CounterApp';


describe('Pruebas a counterApp', () => { 

    test('debe mostrar <CounterApp/> correctamente ', () => { 
    
        const wrapper = shallow(<CounterApp />);
        expect( wrapper ).toMatchSnapshot();
    

     });


     test('debe mostrar el valor por defecto ', () => { 
    
        const  wrapper = shallow(<CounterApp value={ 100 } />);
        const counterText = wrapper.find('h2').text();
        console.log(counterText)
    

     });



 })